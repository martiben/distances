# Distances

Application permetttant de calculer les distances/durées pour faciliter la construction des OM

## Arborescence

```default
root/

├── dist => Projet construit
├── docs => documentation technique
├── src  => sources du projet
```

## Installation :
- Aller sur la page du projet pour télécharger le [logiciel](https://gitlab.com/martiben/distances/raw/master/dist/OM_Distances_Setup_1.0.0.exe)

- Lancer le .exe

- Si le logiciel demande un fichier Excel en entrée => appuyer sur Echap

- Optionnel ! En cas de problème avec le Smartscreen de Windows :  
<img src="https://gitlab.com/martiben/distances/raw/master/docs/img/smartscreen-1.png" width="300" height="281"/>

- Cliquez sur "Informations" et "Exécuter quand même" :  
<img src="https://gitlab.com/martiben/distances/raw/master/docs/img/smartscreen-2.png" width="300" height="279"/>


## Utilisation
1) Vérifier la configuration dans le fichier ```C:\Users\UTILISATEUR\OM-Distances\config.json```.  
Si le fichier n'existe pas, lancer une première fois l'application et quitter (échap) lors de la demande du fichier Excel d'entrée

1) Lancer le raccourci vers l'application

1) Choisir le fichier Excel d'entrée dans la fenêtre

1) Attendre 😄

1) Choisir le fichier Excel de sortie dans la fenêtre

1) C'est prêt !
 