import { app, dialog, Input, ipcMain, Menu, screen } from 'electron';
import * as Store from 'electron-store';
import * as path from 'path';

const excelFilters = [
  { name: 'Excel', extensions: ['xlsx'] },
  { name: 'Excel 2010', extensions: ['xls'] },
  { name: 'All Files', extensions: ['*'] }
];

const omProcessor = require('./om-js/main');
const config = require('./om-js/main').Main.configuration;

app.on('ready', () => {
    console.log('config', config);
    // Get Config
    const configAccessor = getConfig();

    // Get Excel Config or create it
    let excelConf = configAccessor.get('excel', null);
    if (excelConf == null) {
      excelConf = initConfig();
    }

    // Exit if no processor
    if (omProcessor == null || omProcessor.main == null || omProcessor.main.start == null) {
      app.quit();
    }

    // Ask for input file using last path if exists
    let filePath: Array<string> = null;
    try {
      const defPath = configAccessor.get('lastExcelPath', app.getPath('documents'));
      try {
        filePath = dialog.showOpenDialog({
          title: config.app.titles.input_excel,
          defaultPath: defPath,
          filters: excelFilters,
          properties: ['openFile']
        });
      } catch (pErr) {
        // If error, delete last path and ask with 'documents' as path
        configAccessor.delete('lastExcelPath');

        filePath = dialog.showOpenDialog({
          title: config.app.titles.input_excel,
          defaultPath: app.getPath('documents'),
          filters: excelFilters,
          properties: ['openFile']
        });
      }
    } catch (pErr) {
      console.error('Cannot find file', pErr);
    }

    // Check given path
    if (filePath == null || filePath.length !== 1) {
      app.quit();
    }

    const pathFile = filePath[0];
    configAccessor.set('lastExcelPath', pathFile);

    // Calling OM Processor
    omProcessor.main.start(pathFile, excelConf)
      .then(() => {
        app.quit();
      });

  }
);

/** Get Electron Config. */
function getConfig() {
  return new Store({ cwd: path.resolve(app.getPath('home'), 'OM-Distances') })
}

/** Init Electron Config. */
function initConfig(): any {
  const configAccessor = getConfig();

  configAccessor.set('excel.nom_feuille_a_utiliser', 'base');
  configAccessor.set('excel.numero_premiere_ligne_a_lire', 2);

  configAccessor.set('excel.colonne_nom_collaborateur', 'B');
  configAccessor.set('excel.colonne_prenom_collaborateur', 'C');

  configAccessor.set('excel.colonne_adresse_domicile', 'G');
  configAccessor.set('excel.colonne_adresse_affectation', 'E');
  configAccessor.set('excel.colonne_adresse_agence', 'F');

  configAccessor.set('excel.colonne_calcul_domicile_agence', 'J');
  configAccessor.set('excel.colonne_calcul_domicile_affectation', 'K');
  configAccessor.set('excel.colonne_calcul_agence_affectation', 'L');

  configAccessor.set('excel.colonne_calcul_temps_transport_commun_domicile_affectation', 'M');

  return configAccessor.get('excel', null);
}


