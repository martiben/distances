import { CollabInfoClass } from '../../typings/classes/collab-info.class';
import * as Excel from 'exceljs';
import { AllRidesClass } from '../../typings/classes/all-rides.class';
import * as fs from 'fs';
import * as path from 'path';
import { UtilsService } from '../utils.service/utils.service';
import logger from '../../config/logger';
import { dialog } from 'electron';
import { Main } from '../../main';

export class ExcelService {

  /**
   * Read input file to get line to process
   * @param pFilePath
   */
  public static async readInput(pFilePath: string): Promise<Array<CollabInfoClass>> {
    const retour = [];

    let filePath = pFilePath;
    if (pFilePath == undefined || pFilePath === '' || pFilePath.trim() === '') {
      logger.error('[readInput] No file path for Excel.');
      return;
    }

    // Read file
    const workbook = new Excel.Workbook();
    try {
      await workbook.xlsx.readFile(pFilePath);
    } catch {
      logger.error('[readInput] Impossible to read the Excel file');
    }

    // Read Sheet
    let baseSheet = workbook.getWorksheet(Main.configuration.excel.nom_feuille_a_utiliser);
    if (baseSheet == null) {
      logger.error('[readInput] Impossible to find the base Sheet. ' +
        'Please check configuration in src/config/config.ts => config.app.excel.nom_feuille_a_utiliser');
      logger.error('[readInput] => try with the first sheet');
      baseSheet = workbook.getWorksheet(1);
    }

    // Find last row number baseSheet = null
    const firstLineNumber = Main.configuration.excel.numero_premiere_ligne_a_lire;
    let lastIndex = baseSheet.rowCount;
    if (lastIndex == undefined || lastIndex < firstLineNumber) {
      lastIndex = firstLineNumber;
    }

    // For each line => store infos
    for (let i = firstLineNumber; i <= lastIndex; i++) {
      const currentRow = baseSheet.getRow(i);
      const collab = new CollabInfoClass();
      collab.excelLineNumber = i;

      // Get collab name
      const nameVal = currentRow.getCell(Main.configuration.excel.colonne_nom_collaborateur);
      if (nameVal !== undefined && nameVal.value != undefined) {
        collab.name = nameVal.text;
      }

      // Get collab firstname
      const firstnameVal = currentRow.getCell(Main.configuration.excel.colonne_prenom_collaborateur);
      if (firstnameVal !== undefined && firstnameVal.value != undefined) {
        collab.firstname = firstnameVal.text;
      }

      // Get home address
      const homeVal = currentRow.getCell(Main.configuration.excel.colonne_adresse_domicile);
      if (homeVal != undefined && homeVal.value != undefined) {
        collab.homeAddress = homeVal.text;
      }

      // Get Affectation address
      const affVal = currentRow.getCell(Main.configuration.excel.colonne_adresse_affectation);
      if (affVal != undefined && affVal.value != undefined) {
        collab.affectationAddress = affVal.text;
      }

      // Get Agency address
      const agencyVal = currentRow.getCell(Main.configuration.excel.colonne_adresse_agence);
      if (agencyVal != undefined && agencyVal.value != undefined) {
        collab.agencyAddress = agencyVal.text;
      }

      // Add to results if only has all its values
      if (collab.hasValues()) {
        retour.push(collab);
      }
    }
    return retour;
  }

  /**
   * Process results to excel output
   * @param pFilePath
   * @param pResultArray
   */
  static async processOutputForResultInExcel(pFilePath: string, pResultArray: AllRidesClass[]): Promise<void> {
    if (pResultArray === undefined || pResultArray.length === 0) {
      logger.debug('[processOutputForResultInExcel] Nothing to write in Excel');
      return;
    }
    const pathInput = path.parse(pFilePath);
    const proposedPathOuput = pathInput.dir + path.sep + pathInput.name + '-resultat' + pathInput.ext;

    let targetDir: string = null;
    let fullOutPath: string = null;

    if (!!dialog) {
      const dialogRes = dialog.showSaveDialog({
        title: Main.configuration.app.titles.output_excel,
        defaultPath: proposedPathOuput,
        filters: [
          { name: 'Excel', extensions: ['xlsx'] },
          { name: 'Excel 2010', extensions: ['xls'] },
          { name: 'All Files', extensions: ['*'] }
        ]
      });

      if (!dialogRes || dialogRes.length !== 1) {
        logger.debug('[processOutputForResultInExcel] No output path. Using default');
        fullOutPath = proposedPathOuput;
      } else {
        const selectedPath = path.parse(dialogRes[0]);
        targetDir = selectedPath.dir;
        fullOutPath = path.resolve(targetDir, selectedPath.base).toString();
      }
    } else {
      const selectedPath = path.parse('./data/output.xlsx');
      targetDir = selectedPath.dir;
      fullOutPath = path.resolve(targetDir, selectedPath.base).toString();
    }

    // Copy Input Excel to Output
    // Check if input exists
    if (!fs.existsSync(pFilePath)) {
      logger.error('[processOutputForResultInExcel] Le fichier Excel d\'entrée n\'existe pas');
    }

    // Create, if needed, the output folder
    if (targetDir && !fs.existsSync(targetDir)) {
      fs.mkdirSync(targetDir);
    }

    // Copy with overwrite to output
    try {
      fs.copyFileSync(pFilePath, fullOutPath);
    } catch (pErr) {
      logger.error('[readInput] Impossible de créer le fichier Excel de sortie.');
      logger.error('!!!! Merci de Fermer le fichier Excel de sortie !!!!');
      console.error(pErr);
      return;
    }

    // Open the newly created file
    const workbook = new Excel.Workbook();
    try {
      await workbook.xlsx.readFile(fullOutPath);
    } catch {
      logger.error('[readInput] Impossible de lire le fichier Excel de sortie');
    }

    // Read Sheet
    let baseSheet = workbook.getWorksheet(Main.configuration.excel.nom_feuille_a_utiliser);
    if (baseSheet === undefined) {
      logger.error('[readInput] Impossible de trouver la Feuille "base". ' +
        'Merci de vérifier la configuration dans src/config/config.ts => config.app.excel.nom_feuille_a_utiliser');
      logger.error('[readInput] => On essaie quand même avec la première feuille');
      baseSheet = workbook.getWorksheet(1);
    }

    logger.info('Ouverture du fichier Excel de sortie');

    // Change lines
    pResultArray.forEach((aResult: AllRidesClass) => {
      if (!aResult || aResult.collab === undefined || aResult.collab.excelLineNumber === undefined || aResult.collab.excelLineNumber < 0) {
        return;
      }
      logger.info('Ecriture de la ligne du Collaborateur : ' + aResult.collab.fullName + ' sur la ligne ' + aResult.collab.excelLineNumber);

      const row = baseSheet.getRow(aResult.collab.excelLineNumber);
      if (row === undefined) {
        logger.info('Ligne Excel manquante');
        return;
      }

      if (aResult.homeAgencyJourney) {
        // Domicile <-> Agence

        // Distance Aller/Retour Agence-Domicile
        const homeAgency = aResult.homeAgencyJourney;
        const sumDistance = homeAgency.fromToDest.distanceForCarInMeters + homeAgency.destToFrom.distanceForCarInMeters;
        row.getCell(Main.configuration.excel.colonne_calcul_domicile_agence).value = UtilsService.getInKm(sumDistance, false, 3) as number;
      }

      if (aResult.homeAffectationJourney) {
        // Domicile <-> Affectation
        const homeAffectation = aResult.homeAffectationJourney;

        // Distance Aller/Retour Domicile-Affectation
        const sumDistance = homeAffectation.fromToDest.distanceForCarInMeters + homeAffectation.destToFrom.distanceForCarInMeters;
        row.getCell(Main.configuration.excel.colonne_calcul_domicile_affectation).value =
          UtilsService.getInKm(sumDistance, false, 3) as number;

        // Durée Aller uniquement Domicile-Affectation
        if (homeAffectation.fromToDest.durationForTCInSeconds !== undefined) {
          const duration = UtilsService.getInMin(homeAffectation.fromToDest.durationForTCInSeconds, false, 0) as number;
          row.getCell(Main.configuration.excel.colonne_calcul_temps_transport_commun_domicile_affectation).value = duration;
        }
      }

      if (aResult.agencyAffectationJourney) {
        // Agence <-> Affectation
        const agencyAffectation = aResult.agencyAffectationJourney;

        // Distance Aller/Retour Agence-Affectation
        const sumDistance = agencyAffectation.fromToDest.distanceForCarInMeters + agencyAffectation.destToFrom.distanceForCarInMeters;
        row.getCell(Main.configuration.excel.colonne_calcul_agence_affectation).value =
          UtilsService.getInKm(sumDistance, false, 3) as number;
      }
    });

    // Write to Excel file
    logger.info('Ecriture vers le fichier Excel de sortie');
    await workbook.xlsx.writeFile(fullOutPath);
    logger.info('Fichier Excel de sortie prêt !');
  }
}
