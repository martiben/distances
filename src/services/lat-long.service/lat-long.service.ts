import { LatLong } from '../../typings/classes/lat-long.class';
import logger from '../../config/logger';
import * as stringify from 'json-stringify-safe';
import * as rp from 'request-promise';
import { UtilsService } from '../utils.service/utils.service';

export class LatLongService {
  /** Cache for latlong data. */
  private CACHE_LATLONG: Map<string, LatLong> = new Map<string, LatLong>();
  private API_KEY = 'lYrP4vF3Uk5zgTiGGuEzQGwGIVDGuy24';

  /**
   * Get LatLong from string address.
   * @param pAddress
   */
  public async getLatLongForAddress(pAddress: string): Promise<LatLong> {
    let retour = null;
    logger.debug('[getLatLongForAddress] For address : ' + pAddress);

    // Check in cache
    if (this.CACHE_LATLONG.has(pAddress)) {
      logger.debug('[getLatLongForAddress] Get LatLong from cache : ' + pAddress);
      retour = this.CACHE_LATLONG.get(pAddress);
      return retour;
    }

    // Set address on template
    const jsonTemplate: any = {
      location: {
        street: UtilsService.extractStreetFromFullAddress(pAddress),
        city: UtilsService.extractCityFromFullAddress(pAddress),
        adminArea1: 'FR'
      }
    };
    logger.debug('[getLatLongForAddress] JSON Template built : ' + stringify(jsonTemplate));

    // Build JSON for Request
    let jsonDataStr = null;
    try {
      jsonDataStr = JSON.stringify(jsonTemplate);
    } catch (pErr) {
      logger.error('[getLatLongForAddress] Error JSON stringify');
      logger.error(pErr);
      return;
    }
    logger.debug('[getLatLongForAddress] JSON Built : ' + jsonDataStr);

    // HTML Encode JSON
    let jsonDataStrEncoded = null;
    try {
      jsonDataStrEncoded = encodeURIComponent(jsonDataStr);
    } catch (pErr) {
      logger.error('[getLatLongForAddress] Error JSON Encoding');
      logger.error(pErr);
      return;
    }
    logger.debug('[getLatLongForAddress] JSON Encoded : ' + jsonDataStrEncoded);

    // Build Request Data
    const requestData = {
      method: 'POST',
      uri: 'https://www.mapquestapi.com/geocoding/v1/address?key=' + this.API_KEY + '&json=' + jsonDataStrEncoded,
      body: {},
      json: true
    };
    logger.debug('[getLatLongForAddress] Request data : ' + stringify(requestData));

    // Call WS
    let parsedBody = null;
    try {
      parsedBody = await rp(requestData);
    } catch (pErr) {
      logger.error('[getLatLongForAddress] Error on LatLong retieval for : ' + stringify(retour));
      logger.error(pErr);
      return;
    }

    // LatLong Extraction from response
    logger.silly('[getLatLongForAddress] parsedBody : ' + stringify(parsedBody));
    if (parsedBody && parsedBody.results && parsedBody.results.length > 0) {
      const firstItem = parsedBody.results[0];

      if (firstItem.locations && firstItem.locations.length > 0) {
        const result = firstItem.locations[0];

        logger.debug('[getLatLongForAddress] Uppercase from Req : ' + jsonTemplate.location.street.toUpperCase());
        logger.debug('[getLatLongForAddress] Uppercase from Res : ' + result.street.toUpperCase());
        if (result.street.toUpperCase() !== jsonTemplate.location.street.toUpperCase()) {
          logger.debug('[getLatLongForAddress] Differences betweeen "' + jsonTemplate.location.street + '" and "' + result.street + '"');
        }

        if (result.latLng && result.latLng.lat != null && result.latLng.lng != null) {
          const res = new LatLong(result.latLng.lat, result.latLng.lng);
          this.CACHE_LATLONG.set(pAddress, res);
          retour = res;
        }
      }
    }
    logger.debug('[getLatLongForAddress] LatLong found for ' + pAddress + ' : ' + stringify(retour));
    return retour;
  }
}
