import { LatLong } from '../../typings/classes/lat-long.class';
import { VehicleTypeEnum } from '../../typings/enums/vehicle-types.enum';
import { JourneyData } from '../../typings/classes/journey-data.class';
import { FullJourneyData } from '../../typings/classes/full-journey-data.class';
import { MixedJourneyData } from '../../typings/classes/mixed-journey-data.class';
import logger from '../../config/logger';
import * as rp from 'request-promise';
import * as stringify from 'json-stringify-safe';
import { LatLongService } from '../lat-long.service/lat-long.service';
import { UtilsService } from '../utils.service/utils.service';

export class JourneyService {
  /** Accessor to LatLong Service. */
  private latLongService = new LatLongService();

  /** Service URL. */
  private JOURNEY_URL = 'https://routemm.mappy.net/multipath/6.0/routes?clientid=mappy' +
    '&from=%LONG_FROM%,%LAT_FROM%&to=%LONG_DEST%,%LAT_DEST%&providers=';

  /** Tokens. */
  private LONG_FROM_TOKEN = '%LONG_FROM%';
  private LAT_FROM_TOKEN = '%LAT_FROM%';
  private LONG_DEST_TOKEN = '%LONG_DEST%';
  private LAT_DEST_TOKEN = '%LAT_DEST%';

  /**
   * Get journey data (distance/duration) between 2 addresses.
   * @param pFrom
   * @param pDest
   * @param pVehicle
   */
  public async getJourneyData(pFrom: LatLong, pDest: LatLong, pVehicle: VehicleTypeEnum): Promise<JourneyData> {
    let retour = null;

    if (pFrom != null && pDest != null) {
      // Valuate URL
      let url = this.JOURNEY_URL + pVehicle;
      url = url.replace(this.LONG_FROM_TOKEN, pFrom.longitude);
      url = url.replace(this.LAT_FROM_TOKEN, pFrom.latitude);
      url = url.replace(this.LONG_DEST_TOKEN, pDest.longitude);
      url = url.replace(this.LAT_DEST_TOKEN, pDest.latitude);
      logger.debug('[getJourneyData] Valuated URL : ' + url);

      const requestData = {
        method: 'GET',
        uri: url,
        body: {},
        json: true
      };

      // Http Call
      let response = null;
      try {
        response = await rp(requestData);
      } catch (pErr) {
        logger.error('[getJourneyData] Error on JourneyData Http Get');
        logger.error(pErr);
        return retour;
      }
      logger.silly('[getJourneyData] Http Response : ' + stringify(response));

      // Processing response
      if (response != null && response[pVehicle] != null && response[pVehicle].routes && response[pVehicle].routes.length > 0) {

        let itemToProcess = null;
        if (pVehicle === VehicleTypeEnum.CAR) {
          itemToProcess = UtilsService.sortArrayByPropertyAndGetLowest(response[pVehicle].routes, 'length');
        } else if (pVehicle === VehicleTypeEnum.TC) {
          itemToProcess = UtilsService.sortArrayByPropertyAndGetLowest(response[pVehicle].routes, 'time');
        }

        // Extract util data
        if (itemToProcess && itemToProcess.length != null && itemToProcess.time != null) {
          retour = new JourneyData(itemToProcess.length, itemToProcess.time);
        }
      }
    }
    logger.debug('[getLatLongForAddress] JourneyData found for ' + pVehicle + ' from ' + stringify(pFrom) + ' and ' + stringify(pDest)
      + ' : ' + stringify(retour));
    return retour;
  }

  /**
   * Calculate journey between two addresses
   * @param pAddress1
   * @param pAddress2
   */
  public async calculateAddr1AndAddr2Journey(pAddress1: string, pAddress2: string): Promise<FullJourneyData> {
    let retour = new FullJourneyData(pAddress1, pAddress2, new MixedJourneyData(0, 0), new MixedJourneyData(0, 0));

    // Check if same address
    if (pAddress1.toUpperCase() === pAddress2.toUpperCase()) {
      // No need to check distance
      logger.debug('Adresses similaires. Pas de calcul de trajet à effectuer');
    } else {
      // Check LongLat Addr1
      const latLongAddr1 = await this.latLongService.getLatLongForAddress(pAddress1);
      // Check LongLat Addr2
      const latLongAddr2 = await this.latLongService.getLatLongForAddress(pAddress2);

      if (latLongAddr1 == null) {
        logger.warn('[calculateAddr1AndAddr2Journey] Cannot find LatLong for address1 : ' + pAddress1);
        return;
      }
      if (latLongAddr2 == null) {
        logger.warn('[calculateAddr1AndAddr2Journey] Cannot find LatLong for address2 : ' + pAddress1);
        return;
      }

      // Get Distance Addr1 -> Addr2
      const addr1ToAddr2DistanceCar = await this.getJourneyData(latLongAddr1, latLongAddr2, VehicleTypeEnum.CAR);
      const addr1ToAddr2DurationTC = await this.getJourneyData(latLongAddr1, latLongAddr2, VehicleTypeEnum.TC);
      if (addr1ToAddr2DistanceCar == null) {
        logger.debug('[calculateAddr1AndAddr2Journey] Missing car data for journey Aller');
        return;
      }
      let addr1ToAddr2Duration = null;
      if (addr1ToAddr2DurationTC != null && addr1ToAddr2DurationTC.durationInSeconds != null) {
        addr1ToAddr2Duration = addr1ToAddr2DurationTC.durationInSeconds;
      }
      const addr1ToAddr2Journey = new MixedJourneyData(addr1ToAddr2DistanceCar.distanceInMeters, addr1ToAddr2Duration);

      // Get Distance Addr2 -> Addr1
      const addr2ToAddr1DistanceCar: JourneyData = await this.getJourneyData(latLongAddr2, latLongAddr1, VehicleTypeEnum.CAR);
      const addr2ToAddr1DurationTC: JourneyData = await this.getJourneyData(latLongAddr2, latLongAddr1, VehicleTypeEnum.TC);
      if (addr2ToAddr1DistanceCar == null) {
        logger.debug('[calculateAddr1AndAddr2Journey] Missing car data for journey Retour');
        return;
      }
      let addr2ToAddr1Duration = null;
      if (addr2ToAddr1DurationTC != null && addr2ToAddr1DurationTC.durationInSeconds != null) {
        addr2ToAddr1Duration = addr2ToAddr1DurationTC.durationInSeconds;
      }
      const addr2ToAddr1Journey = new MixedJourneyData(addr2ToAddr1DistanceCar.distanceInMeters, addr2ToAddr1Duration);

      retour = new FullJourneyData(pAddress1, pAddress2, addr1ToAddr2Journey, addr2ToAddr1Journey);
    }
    return retour;
  }
}
