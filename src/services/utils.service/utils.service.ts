import * as math from 'mathjs';
import * as moment from 'moment';
import { sortBy } from 'lodash';
import { Main } from '../../main';

export class UtilsService {

  /**
   * Get street (first part) of an address
   * @param pAddress
   */
  public static extractStreetFromFullAddress(pAddress: string): string {
    return UtilsService.extractDataFromAddress(pAddress, false);
  }

  /**
   * Get City (last part) of an address
   * @param pAddress
   */
  public static extractCityFromFullAddress(pAddress: string): string {
    return UtilsService.extractDataFromAddress(pAddress, true);
  }

  /**
   * Get a part of an address using separator
   * @param pAddress
   * @param pLastPart
   */
  private static extractDataFromAddress(pAddress: string, pLastPart: boolean): string {
    let retour = '';
    if (pAddress != null) {

      if (pLastPart) {
        retour = pAddress.substring(pAddress.lastIndexOf(Main.configuration.app.address.separator) + 1, pAddress.length).trim();
      } else {
        retour = pAddress.substring(0, pAddress.lastIndexOf(Main.configuration.app.address.separator)).trim();
      }
    }
    return retour;
  }

  /**
   * Get a distance in meters converted in km.
   * @param pDistanceInMeters
   * @param pWithLabel
   * @param pPrecision
   */
  public static getInKm(pDistanceInMeters: number, pWithLabel: boolean = true, pPrecision: number = 2): string | number {
    let retour: string | number = null;
    if (pDistanceInMeters != null && pDistanceInMeters >= 0) {
      const base = math.unit(pDistanceInMeters, 'm');

      if (base != null) {
        const inKm = base.toNumber('km');
        retour = math.round(inKm, pPrecision) as number;
        if (pWithLabel) {
          retour = retour + ' km';
        }
      }
    }
    return retour;
  }

  /**
   * Get a duration in seconds converted in min.
   * @param pDurationInSeconds
   * @param pWithLabel
   * @param pPrecision
   */
  public static getInMin(pDurationInSeconds: number, pWithLabel: boolean = true, pPrecision: number = 2): string | number {
    let retour: string | number = 0;
    if (pDurationInSeconds != null && pDurationInSeconds > 0) {
      const base = moment.duration(pDurationInSeconds, 'seconds');
      if (base != null) {
        const minutesFull = base.asMinutes();
        if (minutesFull != null) {
          retour = '' + math.round(minutesFull, pPrecision);
          if (pWithLabel) {
            retour += ' min';
          }
        }
      }
    }
    return retour;
  }

  /**
   * Sort an array by a property and get lowest item.
   * @param pArray
   * @param pPropertyToSortBy
   */
  public static sortArrayByPropertyAndGetLowest(pArray: Array<any>, pPropertyToSortBy: string): any {
    let retour = null;

    if (pArray && pArray.length > 0) {
      // Sort Array by property
      const sortedResults = sortBy(pArray, (item: any) => item[pPropertyToSortBy]);

      // Get first
      if (sortedResults && sortedResults.length === pArray.length) {
        retour = sortedResults[0];
      }
    }
    return retour;
  }
}
