import * as winston from 'winston';
import { Main } from '../main';
import { AppConfig } from '../typings/classes/app-config.class';

let logger: winston.Logger;

/** Formatter for messages. */
const formatterTimeStamp = winston.format.printf(pInfo => {
  return `${pInfo.timestamp} ${pInfo.level}: ${pInfo.message}`;
});

/** Format combination. */
const formatLog = winston.format.combine(
  winston.format.timestamp(),
  formatterTimeStamp
);

logger = winston.createLogger({
  level: 'debug',
  format: formatLog,
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: 'distances.log', level: 'debug' }),
  ]
});

export default logger;
