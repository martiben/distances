import * as stringify from 'json-stringify-safe';
import logger from './config/logger';
import { ExcelService } from './services/excel.service/excel.service';
import { JourneyService } from './services/journey.service/journey.service';
import { AllRidesClass } from './typings/classes/all-rides.class';
import { AppConfig } from './typings/classes/app-config.class';
import { CollabInfoClass } from './typings/classes/collab-info.class';

export class Main {

  /** Global App configuration. */
  static configuration: any = {
    app: {
      logs: {
        level: 'debug'
      },
      address: {
        separator: ','
      },
      titles: {
        input_excel: 'Sélectionner le fichier Excel contenant les adresses',
        output_excel: 'Sélectionner le fichier Excel à écrire avec les résultats'
      }
    },
    excel: {}
  };

  /** Accessor to Journey Service. */
  private journeyService = new JourneyService();

  /**
   * Main function
   * @param pExcelPath
   * @param pConfig
   */
  async start(pExcelPath: string, pConfig: AppConfig) {
    logger.info('***************************************************');
    logger.info('************ Lancement du Processus ***************');
    logger.info('***************************************************');

    // Init config
    let conf = pConfig;
    logger.debug('conf : ' + conf);
    if (!conf) {
      conf = new AppConfig();
    }

    // Init excel path
    let excelPath = pExcelPath;
    logger.debug('excelPath : ' + excelPath);
    if (!excelPath) {
      excelPath = './data/input.xlsx';
    }
    logger.info('Recherche sur le fichier : ' + excelPath);

    Main.configuration.excel = conf;
    logger.debug('Using config :' + stringify(Main.configuration));

    // Get all lines from Excel
    const allCollabs: CollabInfoClass[] = await ExcelService.readInput(excelPath);
    logger.info(allCollabs.length + ' Collab' + (allCollabs.length > 1 ? 's' : '')
      + ' trouvés dans le fichier suivant : ' + excelPath);

    // Get LatLong from address
    const resultsArray: AllRidesClass[] = [];

    await Promise.all(allCollabs.map(async (item: CollabInfoClass) => {
      // Trim address to process
      const homeAddress = item.homeAddress.trim();
      const affectationAddress = item.affectationAddress.trim();
      const agencyAddress = item.agencyAddress.trim();

      // Calculate Aller/Retour for Home => Agency
      const homeAgencyDataJourney = await this.journeyService.calculateAddr1AndAddr2Journey(homeAddress, agencyAddress);

      // Calculate Aller/Retour for Home => Affectation
      const homeAffectationDataJourney = await this.journeyService.calculateAddr1AndAddr2Journey(homeAddress, affectationAddress);

      // Calculate Aller/Retour for Agency => Affectation
      const agencyAffectationDataJourney = await this.journeyService.calculateAddr1AndAddr2Journey(agencyAddress, affectationAddress);

      // Storing results
      const result = new AllRidesClass();
      result.collab = item;
      result.homeAgencyJourney = homeAgencyDataJourney;
      result.homeAffectationJourney = homeAffectationDataJourney;
      result.agencyAffectationJourney = agencyAffectationDataJourney;
      resultsArray.push(result);
    }));

    // Output
    await ExcelService.processOutputForResultInExcel(excelPath, resultsArray);
  }
}

export const main = new Main();
