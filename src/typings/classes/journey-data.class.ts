export class JourneyData {
  distanceInMeters: number;
  durationInSeconds: number;

  constructor(pDistance: number, pDuration: number) {
    this.distanceInMeters = pDistance;
    this.durationInSeconds = pDuration;
  }
}
