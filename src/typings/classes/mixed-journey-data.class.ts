export class MixedJourneyData {
  distanceForCarInMeters: number;
  durationForTCInSeconds: number;

  constructor(pDistance: number, pDuration: number) {
    this.distanceForCarInMeters = pDistance;
    this.durationForTCInSeconds = pDuration;
  }
}
