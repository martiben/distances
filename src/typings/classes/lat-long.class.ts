export class LatLong {
  latitude: string;
  longitude: string;

  constructor(pLatitude: string, pLongitude: string) {
    this.latitude = pLatitude;
    this.longitude = pLongitude;
  }
}
