export class AppConfig {
  nom_feuille_a_utiliser: string = 'base';
  numero_premiere_ligne_a_lire: number = 2;

  colonne_nom_collaborateur: string = 'B';
  colonne_prenom_collaborateur: string = 'C';
  colonne_adresse_domicile: string = 'G';
  colonne_adresse_affectation: string = 'E';
  colonne_adresse_agence: string = 'F';

  colonne_calcul_domicile_agence: string = 'J';
  colonne_calcul_domicile_affectation: string = 'K';
  colonne_calcul_agence_affectation: string = 'L';

  colonne_calcul_temps_transport_commun_domicile_affectation: string = 'M';
}
