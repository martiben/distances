import { FullJourneyData } from './full-journey-data.class';
import { CollabInfoClass } from './collab-info.class';

export class AllRidesClass {
  collab: CollabInfoClass;

  homeAgencyJourney: FullJourneyData;
  homeAffectationJourney: FullJourneyData;
  agencyAffectationJourney: FullJourneyData;
}
