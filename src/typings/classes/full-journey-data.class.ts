import { MixedJourneyData } from './mixed-journey-data.class';

export class FullJourneyData {
  fromAddress: string;
  toAddress: string;
  fromToDest: MixedJourneyData;
  destToFrom: MixedJourneyData;

  constructor(pFromAddress: string,pToAddress: string, pFromToDest: MixedJourneyData, pDestToFrom: MixedJourneyData) {
    this.fromAddress = pFromAddress;
    this.toAddress = pToAddress;
    this.fromToDest = pFromToDest;
    this.destToFrom = pDestToFrom;
  }
}
