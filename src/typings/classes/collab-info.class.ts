export class CollabInfoClass {
  excelLineNumber: number;

  name: string;
  firstname: string;

  homeAddress: string;
  affectationAddress: string;
  agencyAddress: string;

  /** Check if instance has all its values. */
  public hasValues(): boolean {
    return this.excelLineNumber != null
      && this.name != null && this.name !== '' && this.name.trim() !== ''
      && this.firstname != null && this.firstname !== '' && this.firstname.trim() !== ''
      && this.homeAddress != null && this.homeAddress !== '' && this.homeAddress.trim() !== ''
      && this.affectationAddress != null && this.affectationAddress !== '' && this.affectationAddress.trim() !== ''
      && this.agencyAddress != null && this.agencyAddress !== '' && this.agencyAddress.trim() !== '';
  }

  public get fullName(): string {
    return this.firstname + ' ' + this.name;
  }
}
