/** Vehicle Kinds. */
export enum VehicleTypeEnum {
  TC = 'tc',
  CAR = 'car'
}
